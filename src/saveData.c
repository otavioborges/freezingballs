/*
 * saveData.c
 *
 *  Created on: 5 de jan de 2020
 *      Author: otavio
 */

#define SAVE_DATA_CONFIG_POS	0x80C0000U
#define SAVE_DATA_BUFFER_POS	0xC007F800U	// End at 0xC0081D80
#define SAVE_DATA_BUFFER_LENGTH	300U

#include "saveData.h"
#include "stm32f7xx.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdint.h>
#include <string.h>

static saveData_rec_t *buffer	= (saveData_rec_t *)SAVE_DATA_BUFFER_POS;
static uint32_t nextPos 		= 0;
static uint32_t firstPos		= 0;
static uint32_t valuesInBuffer	= 0;
static SemaphoreHandle_t listS	= NULL;

void SaveDataInsertRecord(float temp, float setpoint, uint8_t motor, char *time){
	if(listS == NULL)
		listS = xSemaphoreCreateCounting(1,1);

	xSemaphoreTake(listS, portMAX_DELAY);

	buffer[nextPos].currentTemp = temp;
	buffer[nextPos].setpoint = setpoint;
	buffer[nextPos].motorState = motor;
	strcpy(buffer[nextPos].timestamp, time);

	if(valuesInBuffer >= SAVE_DATA_BUFFER_LENGTH){
		firstPos++;
		if(firstPos >= SAVE_DATA_BUFFER_LENGTH)
			firstPos = 0;
	}else if(valuesInBuffer == 0){
		firstPos = nextPos;
		valuesInBuffer++;
	}else{
		valuesInBuffer++;
	}

	nextPos++;
	if(nextPos >= SAVE_DATA_BUFFER_LENGTH)
		nextPos = 0;

	xSemaphoreGive(listS);
}

saveData_rec_t *SaveDataPeekRecord(void){
	if(valuesInBuffer == 0){
		return NULL;
	}else{
		return (buffer + firstPos);
	}
}

void SaveDataPopRecord(void){
	BaseType_t wokenFromISR = pdFALSE;

	if(valuesInBuffer == 0){
		return;
	}else{
		if(listS == NULL)
			listS = xSemaphoreCreateCounting(1,1);

		xSemaphoreTakeFromISR(listS, &wokenFromISR);
		firstPos++;
		if(firstPos >= SAVE_DATA_BUFFER_LENGTH)
			firstPos = 0;

		valuesInBuffer--;
		xSemaphoreGiveFromISR(listS, &wokenFromISR);
	}
}

void SaveDataSaveConfig(float setpoint, float highThreshold, float lowThreshold){
	float *floatValue;

	FLASH->KEYR = 0x45670123;
	FLASH->KEYR = 0xCDEF89AB;

	while(FLASH->SR & FLASH_SR_BSY)
		__asm("nop");

	FLASH->CR &= ~FLASH_CR_SNB;
	FLASH->CR |= (7 << 3) | FLASH_CR_SER;
	FLASH->CR |= FLASH_CR_STRT;
	__DSB();

	// Setpoint
	while(FLASH->SR & FLASH_SR_BSY)
		__asm("nop");

	floatValue = (float *)SAVE_DATA_CONFIG_POS;
	FLASH->CR |= (FLASH_CR_PG | (2 << FLASH_CR_PSIZE_Pos));
	*floatValue = setpoint;
	__DSB();

	// High Th
	while(FLASH->SR & FLASH_SR_BSY)
		__asm("nop");

	floatValue = (float *)(SAVE_DATA_CONFIG_POS + 4);
	FLASH->CR |= (FLASH_CR_PG | (2 << FLASH_CR_PSIZE_Pos));
	*floatValue = highThreshold;
	__DSB();

	// Low Th
	while(FLASH->SR & FLASH_SR_BSY)
		__asm("nop");

	floatValue = (float *)(SAVE_DATA_CONFIG_POS + 8);
	FLASH->CR |= (FLASH_CR_PG | (2 << FLASH_CR_PSIZE_Pos));
	*floatValue = lowThreshold;
	__DSB();

	// wait operation to finish
	while(FLASH->SR & FLASH_SR_BSY)
		__asm("nop");

	// block flash again
	FLASH->CR &= ~FLASH_CR_PG;
	FLASH->CR |= FLASH_CR_LOCK;
}

void SaveDataLoadConfig(float *setpoint, float *highThreshold, float *lowThreshold){
	float *savedValue;

	savedValue = (float *)SAVE_DATA_CONFIG_POS;
	*setpoint = *savedValue;

	savedValue = (float *)(SAVE_DATA_CONFIG_POS + 4);
	*highThreshold = *savedValue;

	savedValue = (float *)(SAVE_DATA_CONFIG_POS + 8);
	*lowThreshold = *savedValue;
}
