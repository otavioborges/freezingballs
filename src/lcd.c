/*
 * lcd.c
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

// GPIOH
#define GPIOH_CLEAR_MASK	0x3C000
#define GPIOH_CLEAR_AFRL	0xF0000000
#define GPIOH_CLEAR_AFRH	0xF

#define GPIOH_SET_MASK		0x28000
#define GPIOH_SET_AFRL		0x40000000
#define GPIOH_SET_AFRH		0x4
// GPIOI
#define GPIOI_CLEAR_MASK	0xFF3C0000
#define GPIOI_CLEAR_AFRH	0xFFFF0FF0

#define GPIOI_SET_MASK		0xA1280000
#define GPIOI_SET_AFRH		0xEE000EE0
// GPIOJ
#define GPIOJ_CLEAR_MASK	0xFFFFFFFF
#define GPIOJ_CLEAR_AFRL	0xFFFFFFFF
#define GPIOJ_CLEAR_AFRH	0xFFFFFFFF

#define GPIOJ_SET_MASK		0xAAAAAAAA
#define GPIOJ_SET_AFRL		0xEEEEEEEE
#define GPIOJ_SET_AFRH		0xEEEEEEEE
// GPIOK
#define GPIOK_CLEAR_MASK	0xFFFF
#define GPIOK_CLEAR_AFRL	0xFFFFFFFF

#define GPIOK_SET_MASK		0xAA6A
#define GPIOK_SET_AFRL		0xEEEE0EEE

// LCD Definitions
#define LCD_WIDTH  480
#define LCD_HEIGHT 272

#define HFP   32
#define HSYNC 40
#define HBP   54

#define VFP   2
#define VSYNC 9
#define VBP   12

#define ACTIVE_LAYER_1		0
#define ACTIVE_LAYER_2		1

// Memory space for layers
#define LCD_LINE_LENGTH		480
#define LCD_MEMORY_LAYER_1	0xC0000000
#define LCD_MEMORY_LAYER_2	0xC003FC00 // end at 0xC007F800

#define ACTIVE_W (HSYNC + LCD_WIDTH + HBP - 1)
#define ACTIVE_H (VSYNC + LCD_HEIGHT + VBP - 1)

#define TOTAL_WIDTH  (HSYNC + HBP + LCD_WIDTH + HFP - 1)
#define TOTAL_HEIGHT (VSYNC + VBP + LCD_HEIGHT + VFP - 1)

#include "lcd.h"
#include "stm32f7xx.h"
#include "font8x8.h"

uint16_t *frameBuffer; //[272][480];
uint8_t activeLayer = ACTIVE_LAYER_2;

static void LCDChangeLayer(void){
	if(activeLayer){
		LTDC_Layer2->CR = LTDC_LxCR_LEN;
		LTDC_Layer1->CR = 0;
	}else{
		LTDC_Layer1->CR = LTDC_LxCR_LEN;
		LTDC_Layer2->CR = 0;
	}

	LTDC->SRCR = LTDC_SRCR_IMR;
	activeLayer ^= 0x01;
}

void LCDInit(void){
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN |
			RCC_AHB1ENR_GPIOIEN | RCC_AHB1ENR_GPIOJEN | RCC_AHB1ENR_GPIOKEN;

	// GPIOE
	GPIOE->MODER   &= ~GPIO_MODER_MODER4;
	GPIOE->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR4;
	GPIOE->PUPDR   &= ~GPIO_PUPDR_PUPDR4;
	GPIOE->AFR[0]  &= ~GPIO_AFRL_AFRL4;

	GPIOE->MODER   |=  (0x02 << GPIO_MODER_MODER4_Pos);
	GPIOE->AFR[0]  |=  (14 << GPIO_AFRL_AFRL4_Pos);

	// GPIOG
	GPIOG->MODER   &= ~GPIO_MODER_MODER12;
	GPIOG->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR12;
	GPIOG->PUPDR   &= ~GPIO_PUPDR_PUPDR12;
	GPIOG->AFR[1]  &= ~GPIO_AFRH_AFRH4;

	GPIOG->MODER   |=  (0x02 << GPIO_MODER_MODER12_Pos);
	GPIOG->AFR[1]  |=  (14 << GPIO_AFRH_AFRH4_Pos);

	// GPIOH
	GPIOH->MODER   &= ~GPIOH_CLEAR_MASK;
	GPIOH->OSPEEDR &= ~GPIOH_CLEAR_MASK;
	GPIOH->PUPDR   &= ~GPIOH_CLEAR_MASK;
	GPIOH->AFR[0]  &= ~GPIOH_CLEAR_AFRL;
	GPIOH->AFR[1]  &= ~GPIOH_CLEAR_AFRH;

	GPIOH->MODER   |=  GPIOH_SET_MASK;
	GPIOH->AFR[0]  |=  GPIOH_SET_AFRL;
	GPIOH->AFR[1]  |=  GPIOH_SET_AFRH;

	// GPIOI
	GPIOI->MODER   &= ~GPIOI_CLEAR_MASK;
	GPIOI->OSPEEDR &= ~GPIOI_CLEAR_MASK;
	GPIOI->PUPDR   &= ~GPIOI_CLEAR_MASK;
	GPIOI->AFR[1]  &= ~GPIOI_CLEAR_AFRH;

	GPIOI->MODER   |=  GPIOI_SET_MASK;
	GPIOI->AFR[1]  |=  GPIOI_SET_AFRH;

	// GPIOJ
	GPIOJ->MODER   &= ~GPIOJ_CLEAR_MASK;
	GPIOJ->OSPEEDR &= ~GPIOJ_CLEAR_MASK;
	GPIOJ->PUPDR   &= ~GPIOJ_CLEAR_MASK;
	GPIOJ->AFR[0]  &= ~GPIOJ_CLEAR_AFRL;
	GPIOJ->AFR[1]  &= ~GPIOJ_CLEAR_AFRH;

	GPIOJ->MODER   |=  GPIOJ_SET_MASK;
	GPIOJ->AFR[0]  |=  GPIOJ_SET_AFRL;
	GPIOJ->AFR[1]  |=  GPIOJ_SET_AFRH;

	// GPIOK
	GPIOK->MODER   &= ~GPIOK_CLEAR_MASK;
	GPIOK->OSPEEDR &= ~GPIOK_CLEAR_MASK;
	GPIOK->PUPDR   &= ~GPIOK_CLEAR_MASK;
	GPIOK->AFR[0]  &= ~GPIOK_CLEAR_AFRL;

	GPIOK->MODER   |=  GPIOK_SET_MASK;
	GPIOK->AFR[0]  |=  GPIOK_SET_AFRL;

	GPIOK->BSRR = GPIO_BSRR_BS_3; // turn backlight by default

	RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;

	LTDC->SSCR = (((HSYNC -1) << 16U) | (VSYNC - 1));
	LTDC->BPCR = (((HBP -1) << 16U) | (VBP - 1));
	LTDC->AWCR = ((ACTIVE_W << 16U) | ACTIVE_H);
	LTDC->TWCR = ((TOTAL_WIDTH << 16U) | TOTAL_HEIGHT);
	LTDC->BCCR = 0;

	frameBuffer = (uint16_t *)LCD_MEMORY_LAYER_1;
//	frameBuffer[1] = (uint16_t *)LCD_MEMORY_LAYER_2;

	LTDC_Layer1->WHPCR = HBP | ((HBP + LCD_WIDTH - 1) << 16);
	LTDC_Layer1->WVPCR = VBP | ((VBP + LCD_HEIGHT - 1) << 16);
	LTDC_Layer1->PFCR = 2; // RGB565
	LTDC_Layer1->CACR = 0xFF;
	LTDC_Layer1->DCCR = 0xFF00FF00;
	LTDC_Layer1->BFCR = 0x606; // using PAxCA for both blending, page 536 of reference
	LTDC_Layer1->CFBAR = (uint32_t)(frameBuffer);
	LTDC_Layer1->CFBLR = ((LCD_WIDTH * 2) << 16) | ((LCD_WIDTH * 2) + 3);
	LTDC_Layer1->CFBLNR = 272;

//	LTDC_Layer2->WHPCR = HBP | ((HBP + LCD_WIDTH - 1) << 16);
//	LTDC_Layer2->WVPCR = VBP | ((VBP + LCD_HEIGHT - 1) << 16);
//	LTDC_Layer2->PFCR = 2; // RGB565
//	LTDC_Layer2->CACR = 0xFF;
//	LTDC_Layer2->DCCR = 0xFF00FF00;
//	LTDC_Layer2->BFCR = 0x606; // using PAxCA for both blending, page 536 of reference
//	LTDC_Layer2->CFBAR = (uint32_t)(frameBuffer[1]);
//	LTDC_Layer2->CFBLR = ((LCD_WIDTH * 2) << 16) | ((LCD_WIDTH * 2) + 3);
//	LTDC_Layer2->CFBLNR = 272;

	GPIOI->BSRR = GPIO_BSRR_BS_12;

	LTDC_Layer1->CR = LTDC_LxCR_LEN;
	LTDC->SRCR = LTDC_SRCR_IMR;
	LTDC->GCR = LTDC_GCR_LTDCEN;	// enable it!
}

void LCDBacklight(uint8_t bclt){
	if(bclt)
		GPIOK->BSRR = GPIO_BSRR_BS_3;
	else
		GPIOK->BSRR = GPIO_BSRR_BR_3;
}

void LCDPaint(uint16_t color){
	for(uint16_t line = 0; line < 272; line++){
		for(uint16_t col = 0; col < 480; col++){
			if(frameBuffer[(line * LCD_LINE_LENGTH) + col] != color)
				frameBuffer[(line * LCD_LINE_LENGTH) + col] = color;
		}
	}
}

void LCDDrawRectangle(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint16_t color){
	for(uint16_t line = 0; line < height; line++){
		for(uint16_t col = 0; col < width; col++){
			if(frameBuffer[((line + y) * LCD_LINE_LENGTH) + (col + x)] != color)
				frameBuffer[((line + y) * LCD_LINE_LENGTH) + (col + x)] = color;
		}
	}
}

void LCDDrawText(char *text, uint16_t color, uint16_t backColor, uint16_t x, uint16_t y){
	// font must be dislocated 0x20
	uint8_t *character;
	uint8_t idx = 0;
	uint16_t posX = x;
	uint16_t posY = y;

	while(text[idx] != '\0'){
		if(idx > 43)
			break;

		character = (uint8_t *)(Font16_Table + ((text[idx] - 0x20) * 32));
		for(uint8_t offset = 0; offset < 32; offset++){
			posX = x + (idx * 16) + ((offset & 0x01) << 3);
			posY = y + (offset >> 1);

			for(uint8_t bit = 0; bit < 8; bit++){
				if((character[offset] >> (7 -bit)) & 0x01){
					if(frameBuffer[(posY * LCD_LINE_LENGTH) + posX] != color)
						frameBuffer[(posY * LCD_LINE_LENGTH) + posX] = color;
				}

				posX++;
			}
		}
		idx++;
	}
}
