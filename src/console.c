/*
 * console.c
 *
 *  Created on: 22 de dez de 2019
 *      Author: otavio
 */

#define MAX_LINES	17
#define MAX_LENGTH	28

#include "FreeRTOS.h"
#include "task.h"
#include "console.h"
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>

#include "lcd.h"

static char g_buffer[MAX_LINES][MAX_LENGTH];
static uint16_t g_firstLine = 0;
static uint16_t g_lineCount = 0;
static uint16_t g_currentLine = 0;
static TaskHandle_t *g_consoleThread;
static uint8_t keepThread = 0;

static void ConsoleThread(void *argument){
	uint16_t textY = 10;

	while(*((uint8_t *)argument)){
		for(uint16_t currentLine = 0; currentLine < g_lineCount; currentLine++){
			textY = (currentLine * 16) + 2;
			LCDDrawRectangle(2, textY, 478, 16, 0);

			if((currentLine + g_firstLine) >= MAX_LINES)
				LCDDrawText(g_buffer[(currentLine + g_firstLine - MAX_LINES)], 0xFFFF, 0, 2, textY);
			else
				LCDDrawText(g_buffer[(currentLine + g_firstLine)], 0xFFFF, 0, 2, textY);
		}

		vTaskDelay(500);
	}
}

void ConsoleInit(void){
	g_firstLine = 0;
	g_lineCount = 0;
	g_currentLine = 0;

	keepThread = 1;
	xTaskCreate(ConsoleThread, "CONSOLE", configMINIMAL_STACK_SIZE * 4, (void *)(&keepThread), configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 2, g_consoleThread);
}

void ConsolePrint(char *b, ...){
	va_list args;
	va_start(args, b);

	vsprintf(g_buffer[g_currentLine], b, args);
	va_end(args);

	g_lineCount++;
	if(g_lineCount >= MAX_LINES){
		g_lineCount = MAX_LINES;
		g_firstLine = g_currentLine + 1;
		if(g_firstLine >= MAX_LINES)
			g_firstLine = 0;
	}

	g_currentLine++;
	if(g_currentLine >= MAX_LINES)
		g_currentLine = 0;
}

void ConsoleClear(void){
	g_firstLine = 0;
	g_lineCount = 0;
	g_currentLine = 0;
}
