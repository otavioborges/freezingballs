/*
 * main.c
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

#define DEFAULT_TARGET_TEMP			18.0
#define DEFAULT_LOW_THRESHOLD		1.5
#define DEFAULT_HIGH_THRESHOLD		1.5

//#define DEFAULT_ETH_SDRAM_ADDR		0xC007F800	// end at 0xC0080000
#define DEFAULT_SERVER_IP			0x36AFAA4B	// Remote
//#define DEFAULT_SERVER_IP			0xC0A80108	// Local

#define FIELD_IDX_SETPOINT			0
#define FIELD_IDX_HIGH_THRESHOLD	1
#define FIELD_IDX_LOW_THRESHOLD		2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "stm32f7xx_it.h"
#include "relay.h"
#include "temperature.h"
#include "lcd.h"
#include "console.h"
#include "rmii.h"
#include "rtc.h"
#include "saveData.h"

#include "eth.h"
#include "ipv4.h"
#include "dhcp.h"
#include "arp.h"
#include "udp.h"
#include "ntp.h"

float temperature = 0;
char message[128];
TaskHandle_t mainThread;
TaskHandle_t ethernetThread;
TaskHandle_t saveThread;
float g_targetSetpoint;
float g_targetLow;
float g_targetHigh;
static uint8_t g_rmiiPacket[2048];

static uint8_t MACAddr[6] = {0x00, 0x22, 0x33, 0x44, 0x55, 0x66};
static uint8_t ntpou = 0;

static void EthThread(void *argument);
static void SaveDataThread(void *argument);
static void MainThread(void *argument);

void NTP_Callback(uint32_t timestamp){
	RtcSetTimestamp(timestamp);
	ntpou = 1;
}

int UDP_ReqCallback(uint8_t *msg, uint16_t length, uint8_t *reply){
	char auxValue[256];
	int charIdx = 0;
	int auxIdx = 0;
	int fieldIdx = 0;

	if(memcmp(msg, "OK_RECORD", 9) == 0){
		SaveDataPopRecord();
		return 0;
	}

	while((msg[charIdx] != '\0') && (charIdx < length)){
		if(msg[charIdx] == ','){
			auxValue[auxIdx] = '\0';

			switch(fieldIdx){
			case FIELD_IDX_SETPOINT:
				g_targetSetpoint = atof(auxValue);
				break;
			case FIELD_IDX_HIGH_THRESHOLD:
				g_targetHigh = atof(auxValue);
				break;
			case FIELD_IDX_LOW_THRESHOLD:
				g_targetLow = atof(auxValue);
				break;
			}

			auxIdx = 0;
			fieldIdx++;
		}else{
			auxValue[auxIdx] = msg[charIdx];
			auxIdx++;
		}

		charIdx++;
	}

	if((g_targetSetpoint == 0) || (g_targetHigh == 0) || (g_targetHigh == 0)){
		g_targetSetpoint = DEFAULT_TARGET_TEMP;
		g_targetLow = (DEFAULT_TARGET_TEMP - DEFAULT_LOW_THRESHOLD);
		g_targetHigh = (DEFAULT_TARGET_TEMP + DEFAULT_HIGH_THRESHOLD);

		strcpy(reply, "ERROR");
	}else{
		SaveDataSaveConfig(g_targetSetpoint, g_targetHigh, g_targetLow);
		g_targetLow = g_targetSetpoint - g_targetLow;
		g_targetHigh = g_targetSetpoint + g_targetHigh;

		strcpy(reply, "OK");
	}

	return strlen(reply);
}

void main(void){
	InitLowLevel();
//	g_rmiiPacket = (uint8_t *)DEFAULT_ETH_SDRAM_ADDR;

	// Check if are values saved in memory
	SaveDataLoadConfig(&g_targetSetpoint, &g_targetLow, &g_targetHigh);
	if(((uint32_t)g_targetSetpoint) == 0){ // NaN
		// use default values
		g_targetSetpoint = DEFAULT_TARGET_TEMP;
		g_targetLow = (DEFAULT_TARGET_TEMP - DEFAULT_LOW_THRESHOLD);
		g_targetHigh = (DEFAULT_TARGET_TEMP + DEFAULT_HIGH_THRESHOLD);
	}else{
		g_targetLow = g_targetSetpoint - g_targetLow;
		g_targetHigh = g_targetSetpoint + g_targetHigh;
	}

	RtcInit();
	LCDInit();
	RelayInit();
	TemperatureInit();
	ConsoleInit();

	LCDPaint(0);
	ConsolePrint("Initiating Ethernet");
	ETHInit(MACAddr);
	ETH_DefineMAC(MACAddr);

	xTaskCreate(MainThread, "MAIN", configMINIMAL_STACK_SIZE * 2, NULL, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3, &mainThread);
	xTaskCreate(EthThread, "ETHERNET", configMINIMAL_STACK_SIZE * 2, NULL, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 4, &ethernetThread);
	xTaskCreate(SaveDataThread, "SAVE", configMINIMAL_STACK_SIZE * 2, NULL, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 5, &saveThread);
	vTaskStartScheduler();

	// Should never reach this point
	NVIC_SystemReset();
}

static void EthThread(void *argument){
	uint32_t dhcpLength = 0;
	uint32_t auxIP = 0;
	uint32_t delay = 0;
	saveData_rec_t *record;
	char msg[64];

	while(IPV4_GetOwnIP() == 0){
		// try to get an IP
		DHCP_RequestIP(g_rmiiPacket, &dhcpLength);
		ETHSend(g_rmiiPacket, dhcpLength);

		Delay(100);
	}

	auxIP = IPV4_GetOwnIP();
	ConsolePrint("IP: %i.%i.%i.%i", ((auxIP & 0xFF000000) >> 24), ((auxIP & 0xFF0000) >> 16), ((auxIP & 0xFF00) >> 8), (auxIP & 0xFF));

	// We have an IP, get the router MAC
	ConsolePrint("Getting gateway data...");
	auxIP = IPV4_GetGateway();
	while(IPV4_HasHost(auxIP) == 0){
		dhcpLength = ARP_CreateRequest(auxIP, NULL, g_rmiiPacket);
		ETHSend(g_rmiiPacket, dhcpLength);
	}

//	auxIP = 0xC0A80108;
//	while(IPV4_HasHost(auxIP) == 0){
//		dhcpLength = ARP_CreateRequest(auxIP, NULL, g_rmiiPacket);
//		ETHSend(g_rmiiPacket, dhcpLength);
//
//		Delay(30000);
//	}

	ConsolePrint("Getting time and date...");
	uint32_t ipdemandacao = 0xC8A007BA;
	while(ntpou == 0){
		dhcpLength = NTP_SendPacket(0xC8A007BA, g_rmiiPacket, 0, 0);
		ETHSend(g_rmiiPacket, dhcpLength);

		Delay(2000);
	}

	ConsolePrint("Uploading data to server...");
	// Router found! Send data
	uint8_t which = 0;
	while(1){
		if(which){
			record = SaveDataPeekRecord();
			if(record != NULL){
				sprintf(msg, "%f,%f,%i,%s,END", TemperatureGet(), g_targetSetpoint, RelayMotorState(), RtcGetTimestamp());
				dhcpLength = UDP_SendPacket(DEFAULT_SERVER_IP, (uint8_t *)msg, 7070, 7070, strlen(msg), g_rmiiPacket, UDP_ReqCallback);
				ETHSend(g_rmiiPacket, dhcpLength);
			}
		}else{
			// query for new states
			sprintf(msg, "STATES");
			dhcpLength = UDP_SendPacket(DEFAULT_SERVER_IP, (uint8_t *)msg, 7070, 7070, strlen(msg), g_rmiiPacket, UDP_ReqCallback);
			ETHSend(g_rmiiPacket, dhcpLength);
		}

		which ^= 1;
		Delay(30000); // wait 1 minute between reports
	}
}

static void SaveDataThread(void *argument){
	while(1){
		Delay(60000);
		SaveDataInsertRecord(TemperatureGet(), g_targetSetpoint, RelayMotorState(), RtcGetTimestamp());
	}
}

static void MainThread(void *argument){
	uint8_t motorState = 0;
	uint8_t printTemp;

	ConsolePrint("Delay first read...");
	vTaskDelay(10000);

	while(1){
		printTemp++;
		if(printTemp >= 100)
			printTemp = 0;

		temperature = TemperatureGet();
		if(printTemp == 0)
			ConsolePrint("Temp: %.1f", temperature);

		if((temperature > g_targetHigh) && (motorState == 0)){
			ConsolePrint("Iniciando motor...");
			motorState = 1;
			RelaySet(RELAY_MOTOR_ON);

			vTaskDelay(30000);
		}else if((temperature < g_targetLow) && (motorState)){
			ConsolePrint("Parando motor...");
			motorState = 0;
			RelaySet(RELAY_MOTOR_OFF);

			vTaskDelay(30000);
		}else{
			vTaskDelay(1000);
		}
	}
}

void ETHReceive(const uint8_t *msg, const uint32_t length){
	int32_t sendLength = ETH_ProcessPacket((uint8_t *)msg, length, g_rmiiPacket);
	if(sendLength > 0){
		ETHSend(g_rmiiPacket, sendLength);
	}
}
