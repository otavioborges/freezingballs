/*
 * rtc.c
 *
 *  Created on: 28 de dez de 2019
 *      Author: otavio
 */

#define RTC_PREDIV_A_VALUE		32
#define RTC_PREDIV_A_FREQ		993
#define RTC_PREDIV_S_VALUE		999				// use this value for 1ms precision of subseconds
#define RTC_PREDIV_S_FREQ		0.992969697

#include <stdio.h>
#include <time.h>

#include "rtc.h"
#include "stm32f7xx.h"

static char rtnValue[32];

static uint8_t BinaryToBCD(uint16_t value){
	uint8_t rtnValue = 0;

	while(value >= 10){
		rtnValue++;
		value -= 10;
	}

	rtnValue = rtnValue << 4; // shift tens
	rtnValue |= (value & 0x0F);

	return rtnValue;
}

void RtcInit(void){
	PWR->CR1 |= PWR_CR1_DBP;

	// Check if RTC has been initialized
	if(RTC->ISR & RTC_ISR_INITS)
		return;

	// enable power to the peripheral
	RCC->BDCR &= ~(RCC_BDCR_RTCSEL_Msk);
	RCC->BDCR |= (0x01 << RCC_BDCR_RTCSEL_Pos) | RCC_BDCR_RTCEN_Msk;	// use LSE, enable RTC

	// diable write-protection to RTC's registers
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;

	// configure the RTC for calendar, using default 32.768kHz xtal and 1kHz resulting CLK on divider A, 1Hz on divider S
	RTC->ISR = RTC_ISR_INIT;
	while((RTC->ISR & RTC_ISR_INITF) == 0)
		__asm("nop");

	RTC->PRER = ((RTC_PREDIV_A_VALUE << RTC_PRER_PREDIV_A_Pos) & RTC_PRER_PREDIV_A_Msk) | ((RTC_PREDIV_S_VALUE << RTC_PRER_PREDIV_S_Pos) & RTC_PRER_PREDIV_S_Msk);
	RTC->CR = 0;

	//TODO: TEST TIME/DATE
	RTC->TR = 0;
	RTC->DR = (1 << RTC_DR_YT_Pos) | (9 << RTC_DR_YU_Pos) | (1 << RTC_DR_WDU_Pos) | (1 << RTC_DR_MU_Pos) | (1 << RTC_DR_DU_Pos);
	// 01/01/2019 00:00:00.000Z

	// reinstate write-protection on RTC
	RTC->ISR &= ~(RTC_ISR_INIT);
	RTC->WPR = 0xFF;
	PWR->CR1 &= ~(PWR_CR1_DBP);
}

char *RtcGetTimestamp(void){
	uint64_t rtnTS = 0;
	uint32_t dateRegValue, timeRegValue;

	RTC->ISR &= ~(RTC_ISR_RSF);
	while((RTC->ISR & RTC_ISR_RSF) == 0)
		__asm("nop");						// wait for shadow register to be synchronized

	// All three register must be read all times
	rtnTS			= 1000 - RTC->SSR;
	timeRegValue	= RTC->TR;
	dateRegValue	= RTC->DR;

	// DO NOT RUN THIS APPLICATION ON the 1900's or after 2099! What are you doing, Marty McFly?
	rtnValue[0] = '2';
	rtnValue[1] = '0';
	rtnValue[2] = ((dateRegValue >> 20) & 0x0F) + '0';
	rtnValue[3] = ((dateRegValue >> 16) & 0x0F) + '0';
	rtnValue[4] = '-';
	rtnValue[5] = ((dateRegValue >> 12) & 0x01) + '0';
	rtnValue[6] = ((dateRegValue >> 8) & 0x0F) + '0';
	rtnValue[7] = '-';
	rtnValue[8] = ((dateRegValue >> 4) & 0x03) + '0';
	rtnValue[9] = (dateRegValue & 0x0F) + '0';
	rtnValue[10] = 'T';
	rtnValue[11] = ((timeRegValue >> 20) & 0x03) + '0';
	rtnValue[12] = ((timeRegValue >> 16) & 0x0F) + '0';
	rtnValue[13] = ':';
	rtnValue[14] = ((timeRegValue >> 12) & 0x07) + '0';
	rtnValue[15] = ((timeRegValue >> 8) & 0x0F) + '0';
	rtnValue[16] = ':';
	rtnValue[17] = ((timeRegValue >> 4) & 0x07) + '0';
	rtnValue[18] = (timeRegValue & 0x0F) + '0';

	sprintf((rtnValue + 19), ".%03i", rtnTS);

	return rtnValue;
}

uint8_t RtcSetTimestamp(uint32_t epoch){
	time_t time = (time_t)epoch;
	struct tm tv;

	localtime_r(&time, &tv);
	return RtcSetTimeDate((tv.tm_year + 1900),(tv.tm_mon + 1), tv.tm_mday, tv.tm_hour, tv.tm_min, tv.tm_sec);
}

uint8_t RtcSetTimeDate(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second){
	if(year < 2000)
		return 0;
	if((month < 1) || (month > 12))
		return 0;
	if((day < 1) || (day > 31))
		return 0;
	if(hour > 23)
		return 0;
	if(minute > 59)
		return 0;
	if(second > 59)
		return 0;

	year -= 2000;
	uint32_t dateValue = (BinaryToBCD(year) << 16) | ((BinaryToBCD(month) & 0x1F) << 8) | (BinaryToBCD(day) & 0x3F);
	uint32_t timeValue = ((BinaryToBCD(hour) & 0x3F) << 16) | ((BinaryToBCD(minute) & 0x3F) << 8) | (BinaryToBCD(second) & 0x3F);
	// diable write-protection to RTC's registers
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;

	// configure the RTC for calendar, using default 32.768kHz xtal and 1kHz resulting CLK on divider A, 1Hz on divider S
	RTC->ISR = RTC_ISR_INIT;
	while((RTC->ISR & RTC_ISR_INITF) == 0)
		__asm("nop");

	// set time and date
	RTC->TR = timeValue;
	RTC->DR = dateValue;

	// reinstate write-protection on RTC
	RTC->ISR &= ~(RTC_ISR_INIT);
	RTC->WPR = 0xFF;

	return 1;
}
