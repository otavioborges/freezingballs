/*
 * eth.c
 *
 *  Created on: 22 de dez de 2019
 *      Author: otavio
 */

// GPIOA
#define GPIOA_CLEAR_MASK		0xC03C
#define GPIOA_CLEAR_AFRL		0xF0000FF0

#define GPIOA_SET_MASK			0x8028
#define GPIOA_SET_AFRL			0xB0000BB0

// GPIOC
#define GPIOC_CLEAR_MASK		0xF0C
#define GPIOC_CLEAR_AFRL		0xFF00F0

#define GPIOC_SET_MASK			0xA08
#define GPIOC_SET_AFRL			0xBB00B0

// GPIOG
#define GPIOG_CLEAR_MASK		0x3CC00000
#define GPIOG_CLEAR_AFRH		0xFF0F000

#define GPIOG_SET_MASK			0x28800000
#define GPIOG_SET_AFRH			0xBB0B000

#define LAN8742A_PHY_ADDRESS	0
/* PHY Reset delay these values are based on a 1 ms Systick interrupt*/
#define PHY_RESET_DELAY                 ((uint32_t)0x00000FFFU)
/* PHY Configuration delay */
#define PHY_CONFIG_DELAY                ((uint32_t)0x00000FFFU)

#define PHY_READ_TO                     ((uint32_t)0x0000FFFFU)
#define PHY_WRITE_TO                    ((uint32_t)0x0000FFFFU)

/* Section 3: Common PHY Registers */

#define PHY_BCR                         ((uint16_t)0x00U)    /*!< Transceiver Basic Control Register   */
#define PHY_BSR                         ((uint16_t)0x01U)    /*!< Transceiver Basic Status Register    */

#define PHY_RESET                       ((uint16_t)0x8000U)  /*!< PHY Reset */
#define PHY_LOOPBACK                    ((uint16_t)0x4000U)  /*!< Select loop-back mode */
#define PHY_FULLDUPLEX_100M             ((uint16_t)0x2100U)  /*!< Set the full-duplex mode at 100 Mb/s */
#define PHY_HALFDUPLEX_100M             ((uint16_t)0x2000U)  /*!< Set the half-duplex mode at 100 Mb/s */
#define PHY_FULLDUPLEX_10M              ((uint16_t)0x0100U)  /*!< Set the full-duplex mode at 10 Mb/s  */
#define PHY_HALFDUPLEX_10M              ((uint16_t)0x0000U)  /*!< Set the half-duplex mode at 10 Mb/s  */
#define PHY_AUTONEGOTIATION             ((uint16_t)0x1000U)  /*!< Enable auto-negotiation function     */
#define PHY_RESTART_AUTONEGOTIATION     ((uint16_t)0x0200U)  /*!< Restart auto-negotiation function    */
#define PHY_POWERDOWN                   ((uint16_t)0x0800U)  /*!< Select the power down mode           */
#define PHY_ISOLATE                     ((uint16_t)0x0400U)  /*!< Isolate PHY from MII                 */

#define PHY_AUTONEGO_COMPLETE           ((uint16_t)0x0020U)  /*!< Auto-Negotiation process completed   */
#define PHY_LINKED_STATUS               ((uint16_t)0x0004U)  /*!< Valid link established               */
#define PHY_JABBER_DETECTION            ((uint16_t)0x0002U)  /*!< Jabber condition detected            */

/* Section 4: Extended PHY Registers */
#define PHY_SR                          ((uint16_t)0x1FU)    /*!< PHY status register Offset                      */

#define PHY_SPEED_STATUS                ((uint16_t)0x0004U)  /*!< PHY Speed mask                                  */
#define PHY_DUPLEX_STATUS               ((uint16_t)0x0010U)  /*!< PHY Duplex mask                                 */

#define PHY_ISFR                        ((uint16_t)0x001DU)    /*!< PHY Interrupt Source Flag register Offset   */
#define PHY_ISFR_INT4                   ((uint16_t)0x000BU)  /*!< PHY Link down inturrupt       */

#include <rmii.h>
#include "stm32f7xx_it.h"
#include "stm32f7xx.h"
#include "FreeRTOS.h"

#define ENET_NBUF     2048
#define ENET_DMA_NRXD   16
#define ENET_DMA_NTXD    8

typedef struct enet_dma_desc{
	volatile uint32_t des0;
	volatile uint32_t des1;
	volatile uint32_t des2;
	volatile uint32_t des3;
} enet_dma_desc_t;

#define ALIGN4 __attribute__((aligned(4)));

static volatile enet_dma_desc_t g_enet_dma_rx_desc[ENET_DMA_NRXD] ALIGN4;
static volatile enet_dma_desc_t g_enet_dma_tx_desc[ENET_DMA_NTXD] ALIGN4;
static volatile uint8_t g_enet_dma_rx_buf[ENET_DMA_NRXD][ENET_NBUF] ALIGN4;
static volatile uint8_t g_enet_dma_tx_buf[ENET_DMA_NTXD][ENET_NBUF] ALIGN4;
static volatile enet_dma_desc_t *g_enet_dma_rx_next_desc = &g_enet_dma_rx_desc[0];
static volatile enet_dma_desc_t *g_enet_dma_tx_next_desc = &g_enet_dma_tx_desc[0];


void ETH_IRQHandler(void){
	ETH->DMASR = 0x1E7FF;

	while(!(g_enet_dma_rx_next_desc->des0 & 0x80000000)){
		const uint8_t *rxp = (const uint8_t *)g_enet_dma_rx_next_desc->des2;
		const uint32_t rxn = (g_enet_dma_rx_next_desc->des0 & 0x3fff0000) >> 16;
		ETHReceive(rxp, rxn);
		g_enet_dma_rx_next_desc->des0 |= 0x80000000; // give it back to the DMA
		// advance the rx pointer for next time
		g_enet_dma_rx_next_desc = (enet_dma_desc_t *)g_enet_dma_rx_next_desc->des3;
	}
}

static void ETHWritePHY(uint16_t phyReg, uint16_t phyValue){
	uint32_t tmpValue = ETH->MACMIIAR;
	tmpValue &= ETH_MACMIIAR_CR;

	// wait for PHY to be free
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB))
		__asm("nop");

	tmpValue |= ((LAN8742A_PHY_ADDRESS << 11U) | ((phyReg & 0x1F) << 6U) | ETH_MACMIIAR_MW | ETH_MACMIIAR_MB);
	ETH->MACMIIDR = (uint32_t)phyValue;
	ETH->MACMIIAR = tmpValue;

	// wait for PHY to be free
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB))
		__asm("nop");
}

static uint16_t ETHReadPHY(uint16_t phyReg){
	uint32_t tmpValue = ETH->MACMIIAR;
	tmpValue &= ETH_MACMIIAR_CR;

	// wait for PHY to be free
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB))
		__asm("nop");

	tmpValue |= ((LAN8742A_PHY_ADDRESS << 11U) | ((phyReg & 0x1F) << 6U) | ETH_MACMIIAR_MB);
	ETH->MACMIIAR = tmpValue;

	// wait for PHY to be free
	while((ETH->MACMIIAR & ETH_MACMIIAR_MB))
		__asm("nop");

	return ETH->MACMIIDR;
}

static void ETHWriteReg(volatile uint32_t *reg, uint32_t value){
	*(reg) = value;
	value = *(reg);
	Delay(1);
	*(reg) = value;
}

void ETHInit(uint8_t *MAC){
	uint16_t phyValue = 0;
	uint32_t macValue = 0;

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIOGEN;

	// GPIOA
	GPIOA->MODER   &= ~GPIOA_CLEAR_MASK;
	GPIOA->OSPEEDR &= ~GPIOA_CLEAR_MASK;
	GPIOA->PUPDR   &= ~GPIOA_CLEAR_MASK;
	GPIOA->AFR[0]  &= ~GPIOA_CLEAR_AFRL;

	GPIOA->MODER   |=  GPIOA_SET_MASK;
	GPIOA->OSPEEDR |=  GPIOA_CLEAR_MASK;
	GPIOA->AFR[0]  |=  GPIOA_SET_AFRL;

	// GPIOC
	GPIOC->MODER   &= ~GPIOC_CLEAR_MASK;
	GPIOC->OSPEEDR &= ~GPIOC_CLEAR_MASK;
	GPIOC->PUPDR   &= ~GPIOC_CLEAR_MASK;
	GPIOC->AFR[0]  &= ~GPIOC_CLEAR_AFRL;

	GPIOC->MODER   |=  GPIOC_SET_MASK;
	GPIOC->OSPEEDR |=  GPIOC_CLEAR_MASK;
	GPIOC->AFR[0]  |=  GPIOC_SET_AFRL;

	// GPIOG
	GPIOG->MODER   &= ~GPIOG_CLEAR_MASK;
	GPIOG->OSPEEDR &= ~GPIOG_CLEAR_MASK;
	GPIOG->PUPDR   &= ~GPIOG_CLEAR_MASK;
	GPIOG->AFR[1]  &= ~GPIOG_CLEAR_AFRH;

	GPIOG->MODER   |=  GPIOG_SET_MASK;
	GPIOG->OSPEEDR |=  GPIOG_CLEAR_MASK;
	GPIOG->AFR[1]  |=  GPIOG_SET_AFRH;

	RCC->AHB1ENR |= RCC_AHB1ENR_ETHMACEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_ETHMACPTPEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_ETHMACRXEN;
	RCC->AHB1ENR |= RCC_AHB1ENR_ETHMACTXEN;

	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	SYSCFG->PMC |= SYSCFG_PMC_MII_RMII_SEL; // turns RMII on

	ETH->DMABMR |= ETH_DMABMR_SR;
	while((ETH->DMABMR & ETH_DMABMR_SR))
		__asm("nop");

	ETH->MACMIIAR = (0x04 << 2U); // setup DRC clock

	ETHWritePHY(PHY_BCR, PHY_RESET);
	Delay(PHY_RESET_DELAY);

	do{
		phyValue = ETHReadPHY(PHY_BSR);
	}while((phyValue & PHY_LINKED_STATUS) != PHY_LINKED_STATUS);

	ETHWritePHY(PHY_BCR, PHY_AUTONEGOTIATION);
	do{
		phyValue = ETHReadPHY(PHY_BSR);
	}while((phyValue & PHY_AUTONEGO_COMPLETE) != PHY_AUTONEGO_COMPLETE);

	phyValue = ETHReadPHY(PHY_SR);
	if((phyValue & PHY_DUPLEX_STATUS) != 0)
		macValue |= ETH_MACCR_DM;

	// is 100M?
	if((phyValue & PHY_SPEED_STATUS) == 0)
		macValue |= ETH_MACCR_FES;

	// Loopback is activated for testing
	macValue |= (0x2000000 | ETH_MACCR_IPCO);
	ETHWriteReg(&(ETH->MACCR), macValue);

	macValue = (ETH_MACFFR_PAM | ETH_MACFFR_PM);
	ETHWriteReg(&(ETH->MACFFR), macValue);

	ETH->MACHTHR = 0;
	ETH->MACHTLR = 0;

	ETHWriteReg(&(ETH->MACFCR), ETH_MACFCR_ZQPD);
	ETHWriteReg(&(ETH->MACVLANTR), 0);

	// Configure Rx/Tx buffers
	for (int i = 0; i < ENET_DMA_NTXD; i++){
		g_enet_dma_tx_desc[i].des0 = 0x00100000 | // set address-chained bit
				0x00c00000 ; // set insert-checksum bits
		g_enet_dma_tx_desc[i].des1 = 0;
		g_enet_dma_tx_desc[i].des2 = (uint32_t)&g_enet_dma_tx_buf[i][0]; // pointer to buf
		if (i < ENET_DMA_NTXD-1)
			g_enet_dma_tx_desc[i].des3 = (uint32_t)&g_enet_dma_tx_desc[i+1]; // chain to next
		else
			g_enet_dma_tx_desc[i].des3 = (uint32_t)&g_enet_dma_tx_desc[0]; // loop to first
	}
	// set up ethernet RX descriptors
	for (int i = 0; i < ENET_DMA_NRXD; i++){
		g_enet_dma_rx_desc[i].des0 = 0x80000000; // set "own" bit = DMA has control
		g_enet_dma_rx_desc[i].des1 = 0x00004000 | // set the RCH bit = chained addr2
				ENET_NBUF; // buffer size in addr1
		g_enet_dma_rx_desc[i].des2 = (uint32_t)&g_enet_dma_rx_buf[i][0];
		if (i < ENET_DMA_NRXD-1)
			g_enet_dma_rx_desc[i].des3 = (uint32_t)&g_enet_dma_rx_desc[i+1];
		else
			g_enet_dma_rx_desc[i].des3 = (uint32_t)&g_enet_dma_rx_desc[0];
	}

	ETH->DMATDLAR = (uint32_t)&g_enet_dma_tx_desc[0]; // point TX DMA to first desc
	ETH->DMARDLAR = (uint32_t)&g_enet_dma_rx_desc[0]; // point RX DMA to first desc

	macValue = (ETH_DMAOMR_RSF | ETH_DMAOMR_TSF | ETH_DMAOMR_OSF);
	ETHWriteReg(&(ETH->DMAOMR), macValue);

	macValue = (ETH_DMABMR_AAB | ETH_DMABMR_FB | (32 << 17U) | (32 << 8U));
	ETHWriteReg(&(ETH->DMABMR), macValue);

	NVIC_SetPriority(ETH_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 4);
	NVIC_EnableIRQ(ETH_IRQn);

	ETH->DMAIER = (ETH_DMAIER_NISE | ETH_DMAIER_RIE);
	ETH->MACA0LR = ((MAC[1] << 8U) | MAC[0] | (MAC[3] << 24U) | (MAC[2] << 16U));
	ETH->MACA0HR = ((MAC[5] << 8U) | MAC[4]);

	macValue = ETH->MACCR;
	macValue |= (ETH_MACCR_RE | ETH_MACCR_TE);
	ETHWriteReg(&(ETH->MACCR), macValue);

	macValue = ETH->DMAOMR;
	macValue |= ETH_DMAOMR_FTF;
	ETHWriteReg(&(ETH->DMAOMR), macValue);

	ETH->DMAOMR |= (ETH_DMAOMR_ST | ETH_DMAOMR_SR);
}

uint32_t ETHSend(uint8_t *msg, uint32_t length){
	__disable_irq();
	  if(g_enet_dma_tx_next_desc->des0 & 0x80000000){
	    __enable_irq();
	    return 0; // if it's set, then we have run out of ringbuffer room. can't tx.
	  }
	  uint8_t *buf = (uint8_t *)g_enet_dma_tx_next_desc->des2;
	  if (length > ENET_NBUF)
	    length = ENET_NBUF; // let's not blow through our packet buffer
	  memcpy(buf, msg, length);
	  g_enet_dma_tx_next_desc->des1  = length;
	  g_enet_dma_tx_next_desc->des0 |= 0x30000000 | // LS+FS = single-buffer packet
	                                   0x80000000 ; // give ownership to eth DMA

	  for(uint8_t delay = 0; delay < 54; delay++)
		  __asm("nop");

	  // see if DMA is stuck because it wasn't transmitting (which will almost
	  // always be the case). if it's stuck, kick it into motion again
	  const volatile uint32_t tps = ETH->DMASR & ETH_DMASR_TPS;
	  if (tps == ETH_DMASR_TPS_Suspended)
	    ETH->DMATPDR = 0; // transmit poll demand = kick it moving again
	  g_enet_dma_tx_next_desc = (enet_dma_desc_t *)g_enet_dma_tx_next_desc->des3;
	  __enable_irq();
}

__WEAK void ETHReceive(const uint8_t *msg, const uint32_t length){
	__asm("nop");
}
