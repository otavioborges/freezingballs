/*
 * relay.c
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

#include "relay.h"
#include "stm32f7xx.h"

void RelayInit(void){
	// Relay connected to D0 (PC7)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;

	GPIOC->MODER &= ~GPIO_MODER_MODER7;
	GPIOC->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR7;
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR7;
	GPIOC->AFR[0] &= ~GPIO_AFRL_AFRL7;

	GPIOC->MODER |= GPIO_MODER_MODER7_0;
	GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR7;
}

void RelaySet(uint8_t mode){
	if(mode == RELAY_MOTOR_ON)
		GPIOC->BSRR = GPIO_BSRR_BS_7;
	else
		GPIOC->BSRR = GPIO_BSRR_BR_7;
}

uint8_t RelayMotorState(void){
	if(GPIOC->ODR & (1 << 7))
		return 1;
	else
		return 0;
}
