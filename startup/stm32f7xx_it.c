// GPIOC
#define GPIOC_CLEAR_MASK		0xC0
#define GPIOC_CLEAR_AFL			0xF000

#define GPIOC_SET_MASK			0x80
#define GPIOC_SET_AFL			0xC000

// GPIOD
#define GPIOD_CLEAR_MASK		0xF03F000F
#define GPIOD_CLEAR_AFL			0xFF
#define GPIOD_CLEAR_AFH			0xFF000FFF

#define GPIOD_SET_MASK			0xA02A000A
#define GPIOD_SET_AFL			0xCC
#define GPIOD_SET_AFH			0xCC000CCC

// GPIOE
#define GPIOE_CLEAR_MASK		0xFFFFC00F
#define GPIOE_CLEAR_AFL			0xF00000FF
#define GPIOE_CLEAR_AFH			0xFFFFFFFF

#define GPIOE_SET_MASK			0xAAAA800A
#define GPIOE_SET_AFL			0xC00000CC
#define GPIOE_SET_AFH			0xCCCCCCCC

// GPIOF
#define GPIOF_CLEAR_MASK		0xFFC00FFF
#define GPIOF_CLEAR_AFL			0xFFFFFF
#define GPIOF_CLEAR_AFH			0xFFFFF000

#define GPIOF_SET_MASK			0xAA800AAA
#define GPIOF_SET_AFL			0xCCCCCC
#define GPIOF_SET_AFH			0xCCCCC000

// GPIOG
#define GPIOG_CLEAR_MASK		0xC0030F0F
#define GPIOG_CLEAR_AFL			0xFF00FF
#define GPIOG_CLEAR_AFH			0xF000000F

#define GPIOG_SET_MASK			0x80020A0A
#define GPIOG_SET_AFL			0xCC00CC
#define GPIOG_SET_AFH			0xC000000C

// GPIOH
#define GPIOH_CLEAR_MASK		0xCC0
#define GPIOH_CLEAR_AFL			0xF0F000

#define GPIOH_SET_MASK			0x880
#define GPIOH_SET_AFL			0xC0C000

// SDRAM Load Mode
#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

#include "stm32f7xx_it.h"
#include "FreeRTOS.h"

static uint32_t g_ticks = 0;

static void RAMSendCommand(uint8_t command, uint16_t mrd, uint8_t nrfs){
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY)
		__asm("nop");

	FMC_Bank5_6->SDCMR = (((mrd & 0x1FFF) << 9U) | ((nrfs & 0x0F) << 5U) | FMC_SDCMR_CTB1 | command);
}

void NMI_Handler(void){
	ErrorHandler(NULL);
}

void HardFault_Handler(void){
	ErrorHandler(NULL);
}

void MemManage_Handler(void){
	ErrorHandler(NULL);
}

void BusFault_Handler(void){
	ErrorHandler(NULL);
}

void UsageFault_Handler(void){
	ErrorHandler(NULL);
}

void DebugMon_Handler(void){
	ErrorHandler(NULL);
}

void TIM7_IRQHandler(void){
	TIM7->SR = 0;
	g_ticks++;
}

void InitLowLevel(void){
	uint16_t tmpCmd;

	// Configure clock and PWR
	PWR->CR1 |= PWR_CR1_DBP;
	RCC->BDCR &= ~RCC_BDCR_LSEDRV;

	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR1 |= PWR_CR1_VOS;

	RCC->CFGR = ((0x7 << RCC_CFGR_MCO1PRE_Pos) | (0x3 << RCC_CFGR_MCO1_Pos) | RCC_CFGR_PPRE2_DIV2 |
			RCC_CFGR_PPRE1_DIV4 | RCC_CFGR_HPRE_DIV1);

	RCC->CR &= ~RCC_CR_PLLON;

	// Turn external crystal
	RCC->CR |= RCC_CR_HSEON;
	while((RCC->CR & RCC_CR_HSERDY) == 0)
		__asm("nop");

	RCC->PLLCFGR = RCC_PLLCFGR_PLLSRC;
	RCC->PLLCFGR |= ((9 << RCC_PLLCFGR_PLLQ_Pos) | (432 << RCC_PLLCFGR_PLLN_Pos) | (25 << RCC_PLLCFGR_PLLM_Pos));

	RCC->CR |= RCC_CR_PLLON;
	while((RCC->CR & RCC_CR_PLLRDY) == 0)
		__asm("nop");

	// Config. PLLSAI1
	RCC->PLLSAICFGR = ((5 << RCC_PLLSAICFGR_PLLSAIR_Pos) | (2 << RCC_PLLSAICFGR_PLLSAIQ_Pos) |
			(3 << RCC_PLLSAICFGR_PLLSAIP_Pos) | (384 << RCC_PLLSAICFGR_PLLSAIN_Pos));
	RCC->DCKCFGR1 |= (2 << RCC_DCKCFGR1_PLLSAIDIVR_Pos);

	RCC->CR |= RCC_CR_PLLSAION;
	while((RCC->CR & RCC_CR_PLLSAIRDY) == 0)
		__asm("nop");

	PWR->CR1 |= PWR_CR1_ODEN;
	while((PWR->CSR1 & PWR_CSR1_ODRDY) == 0)
		__asm("nop");

	PWR->CR1 |= PWR_CR1_ODSWEN;
	while((PWR->CSR1 & PWR_CSR1_ODSWRDY) == 0)
		__asm("nop");

	FLASH->ACR = FLASH_ACR_LATENCY_7WS;

	RCC->CFGR |= 0x2; // SW = 100 use PLL as SystemClock
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL)
		__asm("nop");

	SystemCoreClockUpdate();

	// Initiate RTC on XTAL
	PWR->CR1 |= PWR_CR1_DBP;
	RCC->BDCR |= RCC_BDCR_LSEON;
	while((RCC->BDCR & RCC_BDCR_LSERDY) == 0)
		__asm("nop");

	PWR->CR1 &= ~PWR_CR1_DBP;

	// Initiate RAM
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN |
			RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN;

	// GPIOC
	GPIOC->MODER   &= ~GPIOC_CLEAR_MASK;
	GPIOC->PUPDR   &= ~GPIOC_CLEAR_MASK;
	GPIOC->AFR[0]  &= ~GPIOC_CLEAR_AFL;

	GPIOC->MODER   |=  GPIOC_SET_MASK;
	GPIOC->OSPEEDR |=  GPIOC_CLEAR_MASK;
	GPIOC->AFR[0]  |=  GPIOC_SET_AFL;

	// GPIOD
	GPIOD->MODER   &= ~GPIOD_CLEAR_MASK;
	GPIOD->PUPDR   &= ~GPIOD_CLEAR_MASK;
	GPIOD->AFR[0]  &= ~GPIOD_CLEAR_AFL;
	GPIOD->AFR[1]  &= ~GPIOD_CLEAR_AFH;

	GPIOD->MODER   |=  GPIOD_SET_MASK;
	GPIOD->OSPEEDR |=  GPIOD_CLEAR_MASK;
	GPIOD->AFR[0]  |=  GPIOD_SET_AFL;
	GPIOD->AFR[1]  |=  GPIOD_SET_AFH;

	// GPIOE
	GPIOE->MODER   &= ~GPIOE_CLEAR_MASK;
	GPIOE->PUPDR   &= ~GPIOE_CLEAR_MASK;
	GPIOE->AFR[0]  &= ~GPIOE_CLEAR_AFL;
	GPIOE->AFR[1]  &= ~GPIOE_CLEAR_AFH;

	GPIOE->MODER   |=  GPIOE_SET_MASK;
	GPIOE->OSPEEDR |=  GPIOE_CLEAR_MASK;
	GPIOE->AFR[0]  |=  GPIOE_SET_AFL;
	GPIOE->AFR[1]  |=  GPIOE_SET_AFH;

	// GPIOF
	GPIOF->MODER   &= ~GPIOF_CLEAR_MASK;
	GPIOF->PUPDR   &= ~GPIOF_CLEAR_MASK;
	GPIOF->AFR[0]  &= ~GPIOF_CLEAR_AFL;
	GPIOF->AFR[1]  &= ~GPIOF_CLEAR_AFH;

	GPIOF->MODER   |=  GPIOF_SET_MASK;
	GPIOF->OSPEEDR |=  GPIOF_CLEAR_MASK;
	GPIOF->AFR[0]  |=  GPIOF_SET_AFL;
	GPIOF->AFR[1]  |=  GPIOF_SET_AFH;

	// GPIOG
	GPIOG->MODER   &= ~GPIOG_CLEAR_MASK;
	GPIOG->PUPDR   &= ~GPIOG_CLEAR_MASK;
	GPIOG->AFR[0]  &= ~GPIOG_CLEAR_AFL;
	GPIOG->AFR[1]  &= ~GPIOG_CLEAR_AFH;

	GPIOG->MODER   |=  GPIOG_SET_MASK;
	GPIOG->OSPEEDR |=  GPIOG_CLEAR_MASK;
	GPIOG->AFR[0]  |=  GPIOG_SET_AFL;
	GPIOG->AFR[1]  |=  GPIOG_SET_AFH;

	// GPIOH
	GPIOH->MODER   &= ~GPIOH_CLEAR_MASK;
	GPIOH->PUPDR   &= ~GPIOH_CLEAR_MASK;
	GPIOH->AFR[0]  &= ~GPIOH_CLEAR_AFL;

	GPIOH->MODER   |=  GPIOH_SET_MASK;
	GPIOH->OSPEEDR |=  GPIOH_CLEAR_MASK;
	GPIOH->AFR[0]  |=  GPIOH_SET_AFL;

	RCC->AHB3ENR |= RCC_AHB3ENR_FMCEN;

	FMC_Bank5_6->SDCR[0] = (FMC_SDCR1_RBURST | (2 << 10U) | (2 << 7U) | FMC_SDCR1_NB | (1 << 4U) | (1 << 2U));
	FMC_Bank5_6->SDTR[0] = ((1 << 24u) | (1 << 20U) | (1 << 16U) | (5 << 12U) | (3 << 8U) | (5 << 4U) | 1);

	RAMSendCommand(1, 0,1);
	RAMSendCommand(2,0,1);
	RAMSendCommand(3,0,8);
	tmpCmd = (uint32_t)(SDRAM_MODEREG_BURST_LENGTH_1 | SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL | SDRAM_MODEREG_CAS_LATENCY_2 |
			SDRAM_MODEREG_OPERATING_MODE_STANDARD | SDRAM_MODEREG_WRITEBURST_MODE_SINGLE);
	RAMSendCommand(4, tmpCmd, 1);

	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY)
		__asm("nop");
	FMC_Bank5_6->SDRTR |= ((uint32_t)((1292)<< 1));

	// Initiate TIM6 as tickcounter (1ms precision)
	RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;

	 // TIM6 at 108MHz
	TIM7->CR1 = TIM_CR1_URS;
	TIM7->CR2 = 0;
	TIM7->DIER = TIM_DIER_UIE;
	TIM7->PSC = 1079;	// 100kHz PSC freq.
	TIM7->ARR = 100;	// achieve 1kHz

	NVIC_SetPriority(TIM7_IRQn, 0);
	NVIC_EnableIRQ(TIM7_IRQn);
	TIM7->CR1 |= TIM_CR1_CEN;
}

void Delay(uint32_t ms){
	ms += g_ticks;

	if(ms < g_ticks){
		while(g_ticks > 0)
			__asm("nop");
	}

	// overflow is taken care of
	while(g_ticks <= ms)
		__asm("nop");
}

uint32_t GetTicks(void){
	return g_ticks;
}
void ErrorHandler(void *param){
	__asm("bkpt");
}
