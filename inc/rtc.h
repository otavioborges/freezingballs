/*
 * RTC.h
 *
 *  Created on: 28 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_RTC_H_
#define INC_RTC_H_

#define RTC_DEFAULT_DATETIME_LENGTH	23

#include <stdint.h>

void RtcInit(void);
char *RtcGetTimestamp(void);
uint8_t RtcSetTimestamp(uint32_t epoch);
uint8_t RtcSetTimeDate(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);

#endif /* INC_RTC_H_ */
