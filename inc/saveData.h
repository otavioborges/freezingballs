/*
 * saveData.h
 *
 *  Created on: 5 de jan de 2020
 *      Author: otavio
 */

#ifndef INC_SAVEDATA_H_
#define INC_SAVEDATA_H_

#include "rtc.h"

typedef struct{
	float currentTemp;
	float setpoint;
	uint8_t motorState;
	char timestamp[RTC_DEFAULT_DATETIME_LENGTH];
}saveData_rec_t;

void SaveDataInsertRecord(float temp, float setpoint, uint8_t motor, char *time);
saveData_rec_t *SaveDataPeekRecord(void);
void SaveDataPopRecord(void);
void SaveDataSaveConfig(float setpoint, float highThreshold, float lowThreshold);
void SaveDataLoadConfig(float *setpoint, float *highThreshold, float *lowThreshold);

#endif /* INC_SAVEDATA_H_ */
