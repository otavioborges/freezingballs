/*
 * eth.h
 *
 *  Created on: 22 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_RMII_H_
#define INC_RMII_H_

#include <stdint.h>

void ETHInit(uint8_t *MAC);
uint32_t ETHSend(uint8_t *msg, uint32_t length);
void ETHReceive(const uint8_t *msg, const uint32_t length);

#endif /* INC_RMII_H_ */
