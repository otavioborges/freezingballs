/*
 * console.h
 *
 *  Created on: 22 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_CONSOLE_H_
#define INC_CONSOLE_H_

void ConsoleInit(void);
void ConsolePrint(char *b, ...);
void ConsoleClear(void);

#endif /* INC_CONSOLE_H_ */
