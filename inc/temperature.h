/*
 * temperature.h
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_TEMPERATURE_H_
#define INC_TEMPERATURE_H_

void TemperatureInit(void);
float TemperatureGet(void);

#endif /* INC_TEMPERATURE_H_ */
