/*
 * lcd.h
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_LCD_H_
#define INC_LCD_H_

#include <stdint.h>

void LCDInit(void);
void LCDBacklight(uint8_t bclt);
void LCDPaint(uint16_t color);
void LCDDrawRectangle(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint16_t color);
void LCDDrawText(char *text, uint16_t color, uint16_t backColor, uint16_t x, uint16_t y);

#endif /* INC_LCD_H_ */
