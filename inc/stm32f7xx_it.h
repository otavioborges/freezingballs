#ifndef __STM32F7xx_IT_H
#define __STM32F7xx_IT_H

#include "stm32f7xx.h"

#include <stdlib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif 

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void DebugMon_Handler(void);

void InitLowLevel(void);

void Delay(uint32_t ms);
uint32_t GetTicks(void);
void ErrorHandler(void *param);

#ifdef __cplusplus
}
#endif

#endif /* __STM32F7xx_IT_H */
