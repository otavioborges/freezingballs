/*
 * relay.h
 *
 *  Created on: 21 de dez de 2019
 *      Author: otavio
 */

#ifndef INC_RELAY_H_
#define INC_RELAY_H_

#define RELAY_MOTOR_ON	1
#define RELAY_MOTOR_OFF	0

#include <stdint.h>

void RelayInit(void);
void RelaySet(uint8_t mode);
uint8_t RelayMotorState(void);


#endif /* INC_RELAY_H_ */
